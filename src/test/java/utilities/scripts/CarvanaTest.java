package utilities.scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.Base;
import utilities.Waiter;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

public class CarvanaTest extends Base {

    @Test(testName = "Validate Carvana home page title and url", priority = 1)
    public void validateTitleURL(){

        driver.get("https://www.carvana.com/");
        Assert.assertEquals(driver.getTitle(), "Carvana | Buy & Finance Used Cars Online | At Home Delivery");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/");
    }

    @Test(testName = "Validate the Carvana logo", priority = 2)
    public void validateLogo(){
        driver.get("https://www.carvana.com/");

        Assert.assertTrue(driver.findElement(By.xpath("(//*[@data-qa='icon-wrapper'])[5]")).isDisplayed(), "Logo not displayed");
    }

    @Test(testName = "Validate the main navigation section items", priority = 3)
    public void validateMainNavSec(){
        driver.get("https://www.carvana.com/");

        List<WebElement> navSecs = driver.findElements(By.xpath("//a/*[contains(@data-qa,'menu-title')]"));
        List<String> text = Arrays.asList("HOW IT WORKS", "ABOUT CARVANA", "SUPPORT & CONTACT");
        for (int i = 0; i < navSecs.size()-1; i++) {
            Assert.assertTrue(navSecs.get(i).isDisplayed(), text.get(i) + " is not displayed");
            Assert.assertEquals(navSecs.get(i).getText(), text.get(i), " Element not equal to: " + text.get(i));
        }
    }

    @Test(testName = "Validate the sign in error message", priority = 4)
    public void validateSignInErrorMsg(){
        driver.get("https://www.carvana.com/");
        driver.findElement(By.xpath("//a[@data-cv-test='headerSignInLink']")).click();
        driver.findElement(By.id("usernameField")).sendKeys("johndoe@gmail.com");
        driver.findElement(By.id("passwordField")).sendKeys("abcd1234");
        driver.findElement(By.xpath("//*[@type='submit']")).click();

        Assert.assertEquals(driver.findElement(By.xpath("//*[@data-qa='error-message-container']")).getText(),
                "Email address and/or password combination is incorrect\n" +
                        "Please try again or reset your password.");
    }

    @Test(testName = "Validate the search filter options and search button",priority = 5)
    public void validateFilterOptnsAndSrchBttn(){
    driver.get("https://www.carvana.com/");
    Waiter.pause(5);
        try {
            driver.findElement(By.xpath("//*[contains(text(), 'search all vehicles')]")).click();
        }
        catch (NoSuchElementException a){
            driver.findElement(By.xpath("//*[contains(text(), 'Search All Vehicles')]")).click();
        }    Assert.assertTrue(driver.findElement(By.xpath("//input[@type='text']")).isDisplayed(), "SearchBox not displayed");//srchbox

    List<WebElement> filterOptns = driver.findElements(By.cssSelector("[data-qa='drop-down-wrap']"));
        for (WebElement option:filterOptns) {
            Assert.assertTrue(option.isDisplayed(), option+" is not displayed");
        }

    driver.findElement(By.xpath("//input[@type='text']")) .sendKeys("Tesla");

    Assert.assertTrue(driver.findElement(By.cssSelector("[data-qa='go-button']")).isDisplayed(), "GO button not displayed");
    Assert.assertEquals(driver.findElement(By.cssSelector("[data-qa='go-button']")).getText(), "GO", "GO word not validated");
    }


    @Test(testName = "Validate the search result tiles", priority = 6)
    public void validateSearchResults(){
        driver.get("https://www.carvana.com/");
        try {
            driver.findElement(By.xpath("//*[contains(text(), 'search all vehicles')]")).click();
        }
        catch (Exception a){
            driver.findElement(By.xpath("//*[contains(text(), 'Search All Vehicles')]")).click();
        }

        Waiter.pause(6);
        driver.findElement(By.xpath("//input[@type='text']")).sendKeys("mercedes-benz");
        driver.findElement(By.cssSelector("[data-qa='go-button']")).click();
        Waiter.pause(6);
        Assert.assertTrue(driver.getCurrentUrl().contains("mercedes-benz"));

        List<WebElement> images = driver.findElements(By.xpath("//img[@loading]"));
        List<WebElement> faveButton = driver.findElements(By.className("favorite-icon"));
        List<WebElement> inventoryType = driver.findElements(By.cssSelector("[data-qa='base-inventory-type']"));
        List<WebElement> yearMakeModel = driver.findElements(By.className("year-make-experiment"));
        List<WebElement> trimMileage = driver.findElements(By.className("trim-mileage"));
        List<WebElement> price = driver.findElements(By.xpath("//div[@class='price-variant ']"));
        List<WebElement> monthlyPayment = driver.findElements(By.xpath("//div[@class='monthly-payment']//span"));
        List<WebElement> downPayment = driver.findElements(By.className("down-payment"));
        List<WebElement> delivery = driver.findElements(By.className("delivery"));

        for (int i = 0; i < images.size(); i++) {
            Assert.assertTrue(images.get(i).isDisplayed(), "image "+(i+1)+" is not displayed");

            Assert.assertTrue(faveButton.get(i).isDisplayed(), "faveButton " + (i+1) + " is not displayed");


            Assert.assertTrue(inventoryType.get(i).isDisplayed(), "InventoryType txt " + (i+1) + " is not displayed");
            Assert.assertNotNull(inventoryType.get(i).getText(), "InventoryType txt " + (i + 1) + " is null");

            Assert.assertTrue(yearMakeModel.get(i).isDisplayed(), "yearMakeModel txt " + (i+1) + " is not displayed");
            Assert.assertNotNull(yearMakeModel.get(i).getText(), "yearMakeModel txt " + (i + 1) + " is null");

            Assert.assertTrue(trimMileage.get(i).isDisplayed(), "trimMileage txt " + (i+1) + " is not displayed");
            Assert.assertNotNull(trimMileage.get(i).getText(), "trimMileage txt " + (i + 1) + " is null");

            String num = price.get(i).getText().substring(price.indexOf("$")+2).replaceAll(",", "");
            Assert.assertTrue(Integer.parseInt(num)>0, "price tile " + (i+1) + " is zero or less");

            Assert.assertTrue(monthlyPayment.get(i).isDisplayed(), "monthlyPayment txt " + (i+1) + " is not displayed");
            Assert.assertNotNull(monthlyPayment.get(i).getText(), "monthlyPayment txt " + (i + 1) + " is null");

            Assert.assertTrue(downPayment.get(i).isDisplayed(), "downPayment txt " + (i+1) + " is not displayed");
            Assert.assertNotNull(downPayment.get(i).getText(), "downPayment txt " + (i + 1) + " is null");

            Assert.assertTrue(delivery.get(i).isDisplayed(), "delivery txt " + (i+1) + " is not displayed");
            Assert.assertEquals(delivery.get(i).getText(), "Free Shipping","delivery txt " + (i + 1) + " is null");
        }
    }
}
